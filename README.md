LOGGING 
===
# About

Do logging (in the manner of benchmarking etc) with this application. The application is easy to use and strive to use an ADT that is flexible in many ways. For example, you can easily implement your own functions if you're not looking for an alternative solution, e.g count stuff. Just do like this:

```c
void numeric(unsigned long long *t, ...) {
    va_list vargs;
    va_start(vargs, t);

    int num = va_arg(vargs, int);
    
    if (num % 2)
        num += 10;

    *t = num;

    va_end(vargs);
}
```

and pass it on as the third argument to the ```log_init```-function. Data collected between logent_start and logent_end can again be passed on to the function by adding them after the third argument to ```logent_end```.

## How to
--- more info here

### Initialize one or several logs
--- it will come

## Todo:
Comment out and clean up code in ```log.c``` and ```log.h```.
