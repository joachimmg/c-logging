#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdarg.h>
#include <sys/stat.h>

#ifndef LOG_DIR
#define LOG_DIR     "./logs"
#endif
#ifndef LOG_CMP
#define LOG_CMP     "cmpr.ps"
#endif
#ifndef LOG_PLT
#define LOG_PLT     "cmpr.plot"
#endif

typedef void (*logfunc_t)(void *, ...);

typedef struct stat stat_t;
typedef struct log log_t;
typedef struct logent logent_t;

typedef struct {
    FILE        *f;
    char        *fn;
    int         lines;
    const char  *mode;
} f_t;

struct log {
    f_t         *file;
    char        *xlabel,
                *ylabel;
    logfunc_t   func;
    logent_t    **entry;
    int         entries;
};

struct logent {
    char                *desc;
    f_t                 *file;
    unsigned long long  t1, 
                        t2;
    log_t               *log;
};


void gettime(unsigned long long *t);
log_t *log_init(char *name, const char *mode, char *xlabel, char *ylabel, logfunc_t f);
logent_t *logent_init(log_t *l, char *desc, ...);
void logent_start(logent_t *le, ...);
void logent_end(logent_t *le, int idx, int ent, ...);
void log_destroy(log_t *l);
#endif
