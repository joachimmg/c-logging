Example program
===

Compile the program (you may alter it as well if you'd like) with the following:
```bash
$ make              # make program
$ ./main            # run the program, and wait....
$ make cmp          # make compare files (from data to plots)
```

If you like, you can have evince (must be installed) to view your graph(s), one by one:

```bash
$ make cmpl         # launche pdf in evince
```

TO CLEAN UP:

```bash
$ make clean        # Note! Will remove logs as well!
```
or
```bash
$ make cmpl         # to clean up only the logs
```

### Todo:
Comment ```main.c```.
