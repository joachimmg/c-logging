#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../log.c"

#define LSIZE       100000
#define LSTEP       10000

static int *mk_array(int len) {
    int *arr = (int *)malloc(sizeof(int) * len), i;

    srand(time(NULL));
    for (i = 0; i < len; i++)
        arr[i] = (rand() % (len * 10));

    return arr;
}

static void *cp_array(int *arr, int len) {
    int *narr = (int *)malloc(sizeof(int) * len), i;

    for (i = 0; i < len; i++)
        narr[i] = arr[i];
    
    return narr;
}

static void bubble_sort(int *arr, int len) {
    int c, d, swp;

    for (c = 0; c < (len - 1); c++) {
        for (d = 0; d < (len - c - 1); d++) {
            if (arr[d] > arr[d+1]) {
                swp = arr[d];
                arr[d] = arr[d+1];
                arr[d+1] = swp;
            }
        }
    }
}

static void merge(int *arr, int min, int mid, int max) {
    int l1, l2, i,
        *buf = (int *)malloc(sizeof(int) * (max + 1));

    for (l1 = min, l2 = mid + 1, i = min; l1 <= mid && l2 <= max; i++)
        if (arr[l1] <= arr[l2]) buf[i] = arr[l1++];
        else buf[i] = arr[l2++];

    while (l1 <= mid)
        buf[i++] = arr[l1++];

    while (l2 <= max)
        buf[i++] = arr[l2++];

    for (i = min; i <= max; i++)
        arr[i] = buf[i];

    free(buf);
}

static void merge_sort(int *arr, int min, int max) {
    int mid;
    if (min < max) {
        mid = (min + max) / 2;
        merge_sort(arr, min, mid);
        merge_sort(arr, mid+1, max);
        merge(arr, min, mid, max);
    }
}

int main(int argc, char const *argv[]) {
    /**
     * set first argument to
     * a+:  Append results to current log. Typically if you'd 
     *      make changes to the programs algorithms for each run, and
     *      each execution should be compared.
     * w+:  Each execution does several logging, and create a new log for
     *      this for each run.
     */
    log_t *lt = log_init("actual", "w+", "Elements to sort", "Processing time", (logfunc_t)(gettime)),
          *la = log_init("average", "w+", "Elements to sort", "Avg. processing time", (logfunc_t)(gettime));

    logent_t *le_bt = logent_init(lt, "bubble sort total", "bubble"),
             *le_ba = logent_init(la, "bubble sort avg.", "bubble");

    logent_t *le_mt = logent_init(lt, "merge sort total", "merge"),
             *le_ma = logent_init(la, "merge sort avg.", "merge");

    int k = LSTEP, *arr, *bsort_arr_t, *bsort_arr_a, *msort_arr_t, *msort_arr_a;

    while (k <= LSIZE) {
        arr = mk_array(k);

        bsort_arr_t = cp_array(arr, k);
        bsort_arr_a = cp_array(arr, k);
        msort_arr_t = cp_array(arr, k);
        msort_arr_a = cp_array(arr, k);

        logent_start(le_bt);
        bubble_sort(bsort_arr_t, k);
        logent_end(le_bt, k, 1);

        logent_start(le_ba);
        bubble_sort(bsort_arr_a, k);
        logent_end(le_ba, k, k);

        logent_start(le_mt);
        merge_sort(msort_arr_t, 0, k-1);
        logent_end(le_mt, k, 1);

        logent_start(le_ma);
        merge_sort(msort_arr_a, 0, k-1);
        logent_end(le_ma, k, k);

        k += LSTEP;
    }

    // Closes log-entries
    log_destroy(lt);
    log_destroy(la);

    /*
    Now missing: (will probably be added)
	logfile_avg( log_for );
	logfile_avg( log_while );

	logfile_destroy( log_for );
	logfile_destroy( log_while );
    */

	return 0;
}
